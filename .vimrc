set encoding=UTF-8
syntax on
set expandtab
set shiftwidth=4
set softtabstop=4

set background=dark


"Statusbar

"Colors
"Green Hightlight 1
hi User1 ctermbg=NONE   ctermfg=Green   guibg=NONE  guifg=Green
"Magenta Highlight 2
hi User2 ctermbg=NONE   ctermfg=Magenta guibg=NONE  guifg=Magenta
"Cyan Hightlight 3
hi User3 ctermbg=NONE   ctermfg=Cyan    guibg=NONE  guifg=Cyan

set laststatus=2
set statusline=                             "Empty Start
set statusline+=%1*%<\                         "Cut at start
"Left side
set statusline+=%1*«\                          "Start left block
set statusline+=%2*%n                          "Buffer number
set statusline+=%2*%H                          "Help buffer flag
set statusline+=%2*%M                          "Modified flag
set statusline+=%2*%R                          "Readonly flag
set statusline+=%2*%W                          "Preview window flag
set statusline+=%2*\                         "End flags
set statusline+=%1*×\                          "Separator
set statusline+=%3*%f\                         "File path
set statusline+=%1*»\                          "End left block
"Right side
set statusline+=%1*%=\                         "Change to right side
set statusline+=%1*«\                          "Start right block
set statusline+=%2*%{(&fenc!=''?&fenc:&enc)}\  "File encoding
set statusline+=%1*×\                          "Separator
set statusline+=%2*%((%2l-%2c)%)\              "Line and column
set statusline+=%1*×\                          "Separator
set statusline+=%2*%P\                         "Percentage of file
set statusline+=%1*»\                          "End right block"
