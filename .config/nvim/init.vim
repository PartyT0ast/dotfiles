set encoding=UTF-8
syntax on
set expandtab
set number
set shiftwidth=4
set softtabstop=4
set termguicolors
set noshowmode          "Disables mode in statusline in favor of airline
set background=dark

"Disables The background to enable transparency
highlight Normal guibg=NONE ctermbg=NONE

call plug#begin('~/.local/share/vim/plugged')
    "Basics
    Plug 'scrooloose/nerdtree'
    Plug 'junegunn/goyo.vim'
    Plug 'junegunn/limelight.vim'
    Plug 'Yggdroot/indentLine'

    "UI - Colors and stuff
    Plug 'vim-airline/vim-airline'
    Plug 'rakr/vim-two-firewatch'

    "Languages
    Plug 'rust-lang/rust.vim'
    Plug 'derekwyatt/vim-scala'
call plug#end()

"Function to toggle between 2 and 4 spaces indentation
function ToggleIndent()
    if &shiftwidth==4
        set shiftwidth=2
        set softtabstop=2
    else
        set shiftwidth=4
        set softtabstop=4
    endif
    IndentLinesReset
endfunction


let g:indentLine_enabled=1
let g:indentLine_char='▏'
let g:indentLine_first_char='▏'
let g:indentLine_color_gui="#55606d"
let g:indentLine_showFirstIndentLevel=0
let g:indentLine_faster=1

map <C-n> :NERDTreeToggle<CR>
map <C-f> :Goyo<CR>
map <C-i> :call ToggleIndent()<CR>

"Nerd tree stuff
let NERDTreeShowHidden=1
autocmd StdinReadPre * let s:std_in=1

"Open NERDTree if no files are specified
autocmd VimEnter * if argc() == 0 && !exists("s:std_in") | NERDTree | endif

"Open NERDTree if vim opens a directory
autocmd VimEnter * if argc() == 1 && isdirectory(argv()[0]) && !exists("s:std_in") | exe 'NERDTree' argv()[0] | wincmd p | ene | exe 'cd '.argv()[0] | endif

"Close vim if only NERDTree is open
autocmd BufEnter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif

let g:two_firewatch_italics=1
colorscheme two-firewatch
let g:airline_theme='twofirewatch'
let g:airline_powerline_fonts = 1

