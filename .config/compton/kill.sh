#!/bin/bash

# Terminate already running compton instances
killall -q compton

# Wait until the processes have been shutdown
while pgrep -u $UID -x compton > /dev/null; do sleep 1; done

echo "Compton killed..."
